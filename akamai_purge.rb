#############################################################
# Description:  Initiate a Akamai cache purge               #
# Creation:     3/18/2015                                   #
#############################################################
# Revisions:    3/18/2015 - bherring - Initial Creation     #
#               4/17/2015 - bherring - Added option to send #
#			notification email                  #
#               4/20/2015 - bherring - Cleaned up           #
#                       notifications                       #
#############################################################

require 'rubygems'
require 'rest-client'
require 'optparse'
require 'json'
require 'time'
require 'mail'

Options = Struct.new(
        :cp_code,
        :akamai_username,
        :akamai_password,
        :akamai_domain,
	:akamai_purge_id,
	:mailing_list,
	:notifications_from,
	:notifications_subject
)

class Parser
  def self.parse(options)
    args = Options.new

    opt_parser = OptionParser.new do |opts|
      opts.banner = "Usage: purge_akamai.rb [options]"

      opts.on("-cCPCODE", "--cp-code=CPCODE", "Akamai CP Code to Purge. Seperate multiple with comma ','.") do |c|
        args.cp_code = c
      end

      opts.on("-uUSERNAME", "--akamai-username=USERNAME", "Akamai API Username") do |u|
        args.akamai_username = u
      end

      opts.on("-pPASSWORD", "--akamai-password=PASSWORD", "Akamai API Password") do |p|
        args.akamai_password = p
      end

      opts.on("-dDOMAIN", "--akamai-domain=DOMAIN", "Akamai Domain [staging|production]: OPTIONAL") do |d|
        args.akamai_domain = d
      end

      opts.on("--akamai-purge-id=PURGEID", "Only retrieve and display the Akamai purge status [purge id]: OPTIONAL") do |p|
        args.akamai_purge_id = p
      end

      opts.on("-mMAILINGLIST", "--mailing-listMAILINGLIST", "Mailing List to send notifications to") do |m|
        args.mailing_list = m.split(',')
      end

      opts.on("--notifications-from=NOTIFICATIONSFROM", "The from address that notifications should originate") do |f|
        args.notifications_from = f
      end

      opts.on("--notifications-subject=NOTIFICATIONSSUBJECT", "The subject that the notifications should have") do |s|
        args.notifications_subject = s
      end

      opts.on("-h", "--help", "Prints this help") do
        puts opts
        exit 1
      end
    end

    opt_parser.parse!(options)
    return args
  end
end

# Constants
AKAMAI_API              = 'https://api.ccu.akamai.com'
AKAMAI_API_QUEUE        = '/ccu/v2/queues/default'
AKAMAI_API_PURGES	= '/ccu/v2/purges/'
AKAMAI_TIMEOUT          = 10
AKAMAI_OPEN_TIMEOUT     = 10
AKAMAI_DOMAIN           = 'production'
BASE_SUBJECT		= "Akamai Purge"

# Functions
def display_header_info(cp_code, domain)
        strings = {
                :akamai_info    => 'Akamai Information',
                :cp_code                => "CP Code(s): #{cp_code}",
                :domain                 => "Domain:     #{domain}",
        }
        length = strings.values.max_by(&:length).length

        puts '-' * length
        puts strings[:akamai_info]
        puts '-' * length
        puts '#' * length
        puts strings[:cp_code]
        puts strings[:domain]
        puts '#' * length
end

def format_purge_results(results, cp_code, domain, akamai_api, notification_information = nil)
        strings = {
                :title 			=> 'Akamai Results',
                :cp_code                => "CP Code(s):       #{cp_code}",
                :domain                 => "Domain:           #{domain}",
                :purge_id               => "Purge ID:         #{results['purgeId']}",
		:akamai_api		=> "Akamai API Base:  #{akamai_api}",
                :progress_uri           => "Progress URI:     #{results['progressUri']}",
                :ping_after_seconds     => "ETA:              #{(Time.now - results['pingAfterSeconds']).strftime('%a %b %d %H:%M:%S %Z')}",
                :detail                 => "Result:           #{results['detail']}"
        }
        length = strings.values.max_by(&:length).length

        puts '-' * length
        puts strings[:title]
        puts '-' * length
        puts '#' * length
        puts strings[:cp_code]
        puts strings[:domain]
        puts strings[:purge_id]
        puts strings[:akamai_api]
        puts strings[:progress_uri]
        puts strings[:ping_after_seconds]
        puts strings[:detail]
        puts '#' * length

	send_email(notification_information[:subject] ? notification_information[:subject] : "#{BASE_SUBJECT} - Purge Requested", format_email_body(strings), notification_information) if notification_information
end

def format_status(results, cp_code, domain, akamai_api, notification_information = nil)
        strings = {
                :title 			=> 'Akamai Purge Status',
                :cp_code                => "CP Code(s):       #{cp_code}",
                :domain                 => "Domain:           #{domain}",
                :original_eta		=> "ETA:              #{results['submissionTime'] ? (Time.parse(results['submissionTime']) - results['originalEstimatedSeconds']).strftime('%a %b %d %H:%M:%S %Z') : nil}",
                :completion_time	=> "Completion Time:  #{results['completionTime'] ? Time.parse(results['completionTime']).strftime('%a %b %d %H:%M:%S %Z') : nil}",
                :purge_id               => "Purge ID:         #{results['purgeId']}",
                :submitted_by           => "Submitted By:     #{results['submittedBy']}",
                :purgeStatus            => "Status:           #{results['purgeStatus']}"
        }
        length = strings.values.max_by(&:length).length

        puts '-' * length
        puts strings[:title]
        puts '-' * length
        puts '#' * length
        puts strings[:cp_code]
        puts strings[:domain]
        puts strings[:purge_id]
        puts strings[:original_eta]
        puts strings[:completion_time]
        puts strings[:submitted_by]
        puts strings[:purgeStatus]
        puts '#' * length

	send_email(notification_information[:subject] ? notification_information[:subject] : "#{BASE_SUBJECT} - Purge Status", format_email_body(strings), notification_information) if notification_information
end

def format_failed(message, cp_code, domain, notification_information = nil)
        strings = {
                :title 			=> 'Akamai Request Failed',
                :cp_code                => "CP Code(s):       #{cp_code}",
                :domain                 => "Domain:           #{domain}",
                :message                => "Message:          #{message}"
        }
        length = strings.values.max_by(&:length).length

        puts '-' * length
        puts strings[:title]
        puts '-' * length
        puts '#' * length
        puts strings[:cp_code]
        puts strings[:domain]
        puts strings[:message]
        puts '#' * length

	send_email(notification_information[:subject] ? notification_information[:subject] : "#{BASE_SUBJECT} - Purge Failed", format_email_body(strings), notification_information) if notification_information
end

def purge(cp_code, domain, akamai_username, akamai_password)
	akamai_api 	= "#{AKAMAI_API}#{AKAMAI_API_QUEUE}"
	payload_cpcode  = cp_code.split(',')

	payload = {
		:type       => 'cpcode',
		:objects    => payload_cpcode,
		:domain     => domain,
		:action     => 'remove'
	}

	begin
		response = RestClient::Request.new(
		        :method         => :post,
		        :url            => akamai_api,
		        :user           => akamai_username,
		        :password       => akamai_password,
		        :headers        => {
		                                :accept => :json,
		                                :content_type => :json
		                        },
		        :timeout        => AKAMAI_TIMEOUT,
		        :open_timeout   => AKAMAI_OPEN_TIMEOUT,
		        :payload        => payload.to_json
		).execute

		json = JSON.parse(response.to_str)

		
	rescue Exception => e
		json = nil
		#raise e
	end

	return json
end

def status(purge_id, akamai_username, akamai_password)
	akamai_api = "#{AKAMAI_API}#{AKAMAI_API_PURGES}/#{purge_id}"

	begin
		response = RestClient::Request.new(
		        :method         => :get,
		        :url            => akamai_api,
		        :user           => akamai_username,
		        :password       => akamai_password,
		        :headers        => {
		                                :accept => :json,
		                                :content_type => :json
		                        },
		        :timeout        => AKAMAI_TIMEOUT,
		        :open_timeout   => AKAMAI_OPEN_TIMEOUT
		).execute

		json = JSON.parse(response.to_str)

		
	rescue Exception => e
		json = nil
		#raise e
	end

	return json
end

def format_email_body(string_hash)
	str = "#{string_hash[:title] ? string_hash[:title] : 'Akamai Status'}:"

	string_hash.each do |key,value|
		next if key == :title
		str = "#{str}\n#{value}"
	end

	str
end

def send_email(subject, body, send_info)
	Mail.defaults do
	  delivery_method :smtp, send_info[:smtp]
	end

	send_info[:to].each do |to|
		Mail.deliver do
		       	to to
		     	from send_info[:from]
		  	subject subject
		    	body body
		end
	end
end


options = Parser.parse ARGV

# Validate arguments
begin
        mandatory = [:cp_code, :akamai_username, :akamai_password]


        missing = mandatory.select{ |param| options[param].nil? }
	unless missing.empty?
		puts "Missing options: #{missing.join(', ')}"
		puts options
		exit 2
	end

rescue OptionParser::InvalidOption, OptionParser::MissingArgument
        puts $!.to_s
        puts options
        exit 2
end


domain	= options.akamai_domain
domain	||= AKAMAI_DOMAIN

display_header_info options.cp_code, domain

puts "\n" * 3

# Setup notifications if the user specified a mailing list
if options.mailing_list
	smtp_options = {
		:smtp	=> {
			:address	=> 'localhost',
			:port		=> 25,
		},
		:from		=> options.notifications_from ? options.notifications_from  : 'no-reply@akamai_purge',
		:subject	=> options.notifications_subject ? options.notifications_subject : nil,
		:to		=> options.mailing_list
	}
else
	smtp_options = nil
end

if options.akamai_purge_id
	results = status options.akamai_purge_id, options.akamai_username, options.akamai_password

	format_status results, options.cp_code, domain, AKAMAI_API, smtp_options
else
	puts "Attempting to purge the application with the CP code #{options.cp_code}"

	results = purge options.cp_code, domain, options.akamai_username, options.akamai_password

	if results
		format_purge_results results, options.cp_code, domain, AKAMAI_API, smtp_options
	else
		format_failed 'Status Lookup Failed. Please verify the Purge ID...', options.cp_code, domain, smtp_options
	end
end
